jQuery(document).ready(function(){
	jQuery('.te-popular__slider').slick({
		dots: true,
		infinite: true,
		speed: 750,
		autoplay: false,
		autoplaySpeed: 3500,
		arrows: false,
		slidesToShow: 4,
		slidesToScroll: 1,
		dots: true,
		responsive: [{
			breakpoint: 992,
			settings: {
				slidesToShow: 2,
				slidesToScroll: 1,
				dots: false,
			}
		}, {
			breakpoint: 768,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			}
		}]
	});
	jQuery('.te-brand__slider').slick({
		dots: true,
		infinite: true,
		speed: 750,
		autoplay: false,
		autoplaySpeed: 3500,
		arrows: true,
		slidesToShow: 6,
		slidesToScroll: 1,
		dots: false,
		prevArrow: "<img class='te-prev' src='../img/arrow_prev.png' alt='Santart' title='Santart'>",
		nextArrow: "<img class='te-next' src='../img/arrow_next.png' alt='Santart' title='Santart'>",
		responsive: [{
			breakpoint: 992,
			settings: {
				slidesToShow: 2,
				slidesToScroll: 1
			}
		}, {
			breakpoint: 768,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			}
		}]
	});
	jQuery('.te-productPage__content-imgSmall').slick({
		dots: false,
		infinite: true,
		speed: 750,
		autoplay: false,
		autoplaySpeed: 3500,
		arrows: false,
		slidesToShow: 4,
		slidesToScroll: 1,
		dots: false,
		responsive: [{
			breakpoint: 992,
			settings: {
				slidesToShow: 2,
				slidesToScroll: 1
			}
		}, {
			breakpoint: 768,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			}
		}]
	});
});